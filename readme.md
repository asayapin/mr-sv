# Monorepo SemVer

Simple tool for semantic versioning in monorepository

[![npm version](https://badge.fury.io/js/mr-sv.svg)](https://badge.fury.io/js/mr-sv)
[![pipeline status](https://gitlab.com/asayapin/mr-sv/badges/master/pipeline.svg)](https://gitlab.com/asayapin/mr-sv/-/commits/master) 

## Idea

Say you have a repo with several projects, each with it's own version. All the projects
share same CI pipeline, and you want to generate packages, but don't want to manually update versions.

The tool is intended to:
- compute version for each component using commit history,
  - get repo commit log
  - convert it to several logs, each corresponding to individual component
  - calculate version
- invoke build command for each component

It's mostly mimics SemVer functionality considering repository with several projects

## Installation

`npm install mr-sv`

Fresh version is available on npm

## Usage

The tool can:
- create sample config: `node mr-sv init`
- process current repository: `node mr-sv run`

Help is shown using `node mr-sv help`

### Usage in git repository

As with semver, this tool assumes that every commit in history is marked with specific string,
used to determine version:

`[<vspec>|<pspec>]`

where:
- `<vspec>` is version specifier: one of specified in `mr-sv` config
- `<pspec>` is project specifier: on of following:
  - project's folder name
  - `*` to bump version for all projects
  - `**` to bump version for all touched projects

Example:

Considering following config:

```json
{
    // ...
    "versionSpecifiers": {
        "major": [
            "new"
        ],
        "minor": [
            "feat"
        ],
        "patch": [
            "fix"
        ],
        "override": [
            "ovr"
        ]
    }
}
```

Initial version: 1.0.0

`mr-sv` marker  | touched files | new version for project p1
----------------|---------------|---------------------------
[new\|p1]       | p1/a.js       | 2.0.0.0
[feat\|p1]      | p1/a.js       | 1.1.0.0
[fix\|p1]       | p1/a.js       | 1.0.1.0
[fix\|**]       | p1/a.js       | 1.0.1.0
[fix\|*]        | p2/a.js       | 1.0.1.0
[ovr\|p1:3.0.0] | p1/a.js       | 3.0.0.0
&lt;none given&gt;| p1/a.js    | 1.0.0.1