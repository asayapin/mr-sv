import { readFileSync } from 'fs';
import { glob } from 'glob';
import RootService from './services/root';
import VersionResolver from './services/version-resolver';
import ArgsService from './services/args-service';
import HelpService from './services/help-service';
import InitService from './services/init-service';
import MainService from './services/main-service';
import ConfigService from './services/config-service';
import ProjectsResolverService from './services/projects-resolver';
import GitService from './services/git';

export const RootServiceFactory = (): RootService => {
    const argsService = new ArgsService(() => process.argv);
    const configService = new ConfigService(argsService, s => readFileSync(s, 'utf-8'));
    return new RootService(
        argsService,
        new HelpService(),
        new MainService(
            new VersionResolver(
                configService, 
                new ProjectsResolverService(glob.sync), 
                new GitService()), 
            configService,
            argsService),
        new InitService()
    )
};
