export class Version {
  major: number;
  minor: number;
  patch: number;
  build: number;
  chunk: string;
  constructor(version: string) {
    const chunks = version.split(/\.|\-/g);
    this.major = parseInt('0' + chunks[0], 10);
    this.minor = parseInt(chunks[1] ?? '0', 10);
    this.patch = parseInt(chunks[2] ?? '0', 10);
    this.build = parseInt(chunks[3] ?? '0', 10);
    this.chunk = chunks[4] ?? '';
  }

  public incMajor() {
    this.minor = this.patch = this.build = 0;
    this.major += 1;
  }

  public incMinor() {
    this.patch = this.build = 0;
    this.minor += 1;
  }

  public incPatch() {
    this.build = 0;
    this.patch += 1;
  }

  public incBuild() {
    this.build += 1;
  }

  public toString(dropBuild?: boolean): string {
    return `${this.major}.${this.minor}.${this.patch}${dropBuild ? '' : ('.' + this.build)}${this.chunk.length > 0 ? `-${this.chunk}` : ''}`;
  }
}
