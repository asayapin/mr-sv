import { Version } from './version';

export type ProjectFiles = { [key: string]: string[] };
export type ProjectHistory = { [key: string]: string[] };
export type ProjectVersions = { [key: string]: Version };
