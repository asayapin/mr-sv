export interface Projects {
  include: string[];
  exclude: string[];
  shared: string[];
}

export interface VersionSpecifiers {
  major: string[];
  minor: string[];
  patch: string[];
  override: string[];
}

export interface Config {
  projects: Projects;
  commands: string[];
  versionSpecifiers: VersionSpecifiers;
  defaultVersion: string;
}
