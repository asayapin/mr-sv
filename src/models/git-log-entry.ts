export interface GitLogEntry {
  text: string;
  files: string[];
}
