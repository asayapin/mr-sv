export class Args {
  constructor(source: any) {
    this.config = source?.config ?? 'config.json';
    this.command = source?.command ?? 'help';
    this.dropBuildNumber = !(source?.dropBuildNumber === undefined);
  }
  
  dropBuildNumber: boolean;
  public config: string;
  public command: Command;
}

const Command = ['run', 'init', 'help'] as const;
const opts = ['-c', '--config'] as const;
const switches = ['--drop-build-number'] as const;
const optsNames = {
  '-c': 'config',
  '--config': 'config',
} as const;
const switchesNames = {
  '--drop-build-number': 'dropBuildNumber'
};

export type Command = typeof Command[number];
export type Option = typeof opts[number];
export type Switch = typeof switches[number];

export const isCommand = (x: string): x is Command => Command.indexOf(x as Command) !== -1;
export const isOption = (x: string): x is Option => opts.indexOf(x as Option) !== -1;
export const getOptionName = (x: Option) => optsNames[x];
export const isSwitch = (x: string): x is Switch => switches.indexOf(x as Switch) !== -1;
export const getSwitchName = (x: Switch) => switchesNames[x];