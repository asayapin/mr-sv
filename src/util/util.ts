import { exit } from 'process';

export const exitIfTrue = (cond: boolean, message: string) => {
  if (cond) {
    console.log(message);
    exit(1);
  }
};

export const throwIfTrue = (cond: boolean, message: string) => {
  if (cond) throw new Error(message);
};
