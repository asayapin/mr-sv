import { exec } from 'child_process';
import { writeFileSync } from 'fs';
import { Config } from '../models/config';
import { ProjectVersions } from '../models/project-files';
import { IConfigService } from './config-service';
import { IVersionResolver } from './version-resolver';
import * as format from 'string-format';
import { IArgsService } from './args-service';
import { Args } from '../models/args';

class MainService {
  constructor(private versionResolver: IVersionResolver, 
              private configService: IConfigService,
              private argsService: IArgsService) { }

  async run(): Promise<void> {
    const version = await this.versionResolver.resolveVersions();
    const config = this.configService.getConfig();
    const args = this.argsService.getArgs();
    this.exportEnvironmentVariables(version, args);
    this.runScript(version, config, args);
  }

  private encodeVarName = (s: string) => s.replace(/[^A-Za-z0-9]/g, '_');

  private exportEnvironmentVariables(versions: ProjectVersions, args: Args) {
    writeFileSync(
      '.env',
      Object.keys(versions)
        .map((v) => `MR_SV_${this.encodeVarName(v)}=${versions[v].toString(args.dropBuildNumber)}`)
        .join('\n'),
    );
  }

  private runScript(versions: ProjectVersions, config: Config, args: Args) {
    Object.keys(versions).forEach((k) => {
      config.commands.forEach((c) => {
        exec(format(c, k, versions[k].toString(args.dropBuildNumber)), console.log);
      });
    });
  }

  public static getHelp(): string {
    return `RUN command
        
Process current repository: build projects versions, export them to .env, run shell commands

Flow:
- get all projects in current repo, according to config,
- get all files belonging to project,
- get commit history,
- link every commit to 0+ project,
- for every project, get it's private history,
- for every commit in history, determine version update (default - build version increment),
- write built versions to .env file,
- run scripts from config formatted with project name and version

Project name is assumed to be name of folder containing file resolved as project root:
E.g. if config specifies project root file ('projects/include' node) as package.json file,
and this files are found in repo root, and in folder ./myProject, then resolved project
names are '.' and 'myProject'

Arguments:
    --drop-build-number     remove build number from version string (useful for npm)`;
  }
}

export default MainService;