import { Args, getOptionName, getSwitchName, isCommand, isOption, isSwitch } from '../models/args';
import { throwIfTrue } from '../util/util';

interface ParseStepResult {
  args: string[];
  name: string;
  value: string;
}

function newResult(args: string[], c: number, name: string, value: string = ''): ParseStepResult{
  return {
    args: args.slice(c),
    name,
    value
  };
}

export abstract class IArgsService {
  abstract getArgs(): Args;
}

class ArgsService implements IArgsService {
  constructor(private argsProvider: () => string[]) { }

  getArgs(): Args {
    return ArgsService.parseArgs(this.argsProvider());
  }

  static getOptionValue(args: string[], optionName: string, optionKey: string) {
    throwIfTrue(args.length < 2 || args[1].startsWith('-'), `value not supplied for option ${optionKey}`);
    return newResult(args, 2, optionName, args[1]);
  }

  static parseTop(args: string[]): ParseStepResult {
    const a = [...args];
    if (isCommand(a[0]))
      return newResult(a, 1, 'command', a[0]);
    if (isOption(a[0])) return ArgsService.getOptionValue(a, getOptionName(a[0]), a[0]);
    if (isSwitch(a[0])) return newResult(a, 1, getSwitchName(a[0]));
    return newResult(a, 1, '');
  }

  static parseArgs(args: string[]) {
    let a = [...args];
    const ret: any = {};
    while (a.length > 0) {
      const step = ArgsService.parseTop(a);
      if (a.length === step.args.length) break;
      ret[step.name] = step.value;
      a = step.args;
    }
    return new Args(ret);
  }
}

export default ArgsService;