import { GitLogEntry } from '../models/git-log-entry';
import simpleGit, { DefaultLogFields, ListLogLine, ListLogSummary, SimpleGit } from 'simple-git';

export interface IGitService {
  log(): Promise<GitLogEntry[]>;
}

class GitService implements IGitService {
  git: SimpleGit;

  constructor() {
    this.git = simpleGit();
  }

  log(): Promise<GitLogEntry[]> {
    return this.git.log({ '--stat': null }).then((v) =>
      v.all.map((l) => ({
        text: `${l.message}\n${l.body}`,
        files: l.diff?.files.map((f) => f.file) ?? [],
      })),
    );
  }
}

export default GitService;