import { Config } from '../models/config';
import { throwIfTrue } from '../util/util';
import { IArgsService } from './args-service';

export interface IConfigService {
  getConfig(): Config;
}

class ConfigService implements IConfigService {
  constructor(private argsParser: IArgsService, 
              private fileReader: (path: string) => string) { }

  getConfig() {
    const args = this.argsParser.getArgs();
    const contents = this.fileReader(args.config);
    const config = JSON.parse(contents) as Config;
    throwIfTrue(config === null || config === undefined, 'incorrect config provided');
    return config;
  }
}

export default ConfigService;