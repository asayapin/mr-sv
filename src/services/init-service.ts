import { writeFileSync } from 'fs';
import { Args } from '../models/args';
import { Config } from '../models/config';

class InitService {
  writeSampleConfig(args: Args): Promise<void> {
    const sample: Config = {
      commands: ['sample command with path={0} and vesion={1}'],
      defaultVersion: '1.0.0.0',
      projects: {
        include: ['**/package.json'],
        exclude: ['node_modules/**/*'],
        shared: ['any_shared_path_reference'],
      },
      versionSpecifiers: {
        major: ['new-version'],
        minor: ['feat', 'feature'],
        patch: ['fix', 'patch'],
        override: ['ovr'],
      },
    };
    writeFileSync(args.config, JSON.stringify(sample));
    return Promise.resolve();
  }

  public static getHelp(): string {
    return `INIT command
        
Writes a sample config containing all required nodes

Config structure:
{
    commands - array of command format string with two positional placeholders - {0} for path, {1} for version
    defaultVersion - initial project version
    versionSpecifiers - object with string->string[] mapping from version to keywords
    projects - collection of path specifiers for projects searching
        include - array of path specifiers of files to include as projects (e.g. csproj for C#, package.json for JS, etc)
        exclude - path specifiers that should be excluded from search
        shared - path specifiers for files that are shared between all projects
}

Version specifiers work the following way:
    given commit text (message + body) contains string of pattern '[<vspec>|<projectName>]'
    then version for <projectName> is increased according to version that <vspec> belongs to
For example, if config contains vspec 'feat' for minor version, then commit text with '[feat|myProj]'
will increase minor version for myProj`;
  }
}

export default InitService;