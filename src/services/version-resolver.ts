import { Config } from '../models/config';
import { GitLogEntry } from '../models/git-log-entry';
import { ProjectFiles, ProjectHistory, ProjectVersions } from '../models/project-files';
import { Version } from '../models/version';
import { IConfigService } from './config-service';
import { IGitService } from './git';
import { IProjectsResolverService } from './projects-resolver';

export interface IVersionResolver {
  resolveVersions(): Promise<ProjectVersions>;
}

class VersionResolver implements IVersionResolver {
  constructor(private environmentService: IConfigService, 
              private projectsResolver: IProjectsResolverService, 
              private git: IGitService) { }

  async resolveVersions(): Promise<ProjectVersions> {
    const config = this.environmentService.getConfig();
    const projects = this.projectsResolver.getProjects(config);
    const log = await this.git.log();
    const projectsHistories = this.splitCommitsBetweenProjects(log, projects);
    return this.buildVersions(config, projectsHistories);
  }

  private belongsToProject(projName: string, projFiles: string[]){
    return (e : GitLogEntry) : boolean => {
      const containsFile = e.files.some(f => projFiles.includes(f));
      const commonCommit = /\[.*\|\*\]/g.test(e.text);
      return containsFile || commonCommit;
    }
  }

  private splitCommitsBetweenProjects(log: GitLogEntry[], projects: ProjectFiles): ProjectHistory {
    return Object.keys(projects).reduce<ProjectHistory>((acc, curr) => {
      const proj = projects[curr];
      const commits = log.filter(this.belongsToProject(curr, proj)).map((l) => l.text);
      acc[curr] = commits.reverse();
      return acc;
    }, {});
  }

  private buildVersions(config: Config, projects: ProjectHistory): ProjectVersions {
    return Object.keys(projects).reduce<ProjectVersions>((acc, curr) => {
      acc[curr] = this.buildProjectVersion(curr, projects[curr], config);
      return acc;
    }, {});
  }

  private buildProjectVersion(project: string, history: string[], config: Config): Version {
    let ver = new Version(config.defaultVersion);
    const vs = config.versionSpecifiers;
    const regexBuilder = this.buildRegex(project);
    const ovrBuilder = this.buildOverrideRegex(project);
    const override = vs.override.map(ovrBuilder);
    const patch = vs.patch.map(regexBuilder);
    const minor = vs.minor.map(regexBuilder);
    const major = vs.major.map(regexBuilder);
    history.forEach((curr) => {
      const test = (x: RegExp) => x.test(curr);
      ver.incBuild();
      if (patch.some(test)) ver.incPatch();
      if (minor.some(test)) ver.incMinor();
      if (major.some(test)) ver.incMajor();
      const ovr = override.filter(test).map(r => r.exec(curr)?.groups?.newver);
      if (ovr.length > 0 && ovr[0] !== undefined)
        ver = new Version(ovr[0]);
    });
    return ver;
  }

  private buildOverrideRegex(project: string){
    return (spec: string) => {
      return new RegExp(`\\[${this.sanitize(spec)}\\|(.*,)?${this.sanitize(project)}\\:(?<newver>\\d+.\\d+.\\d+(.\\d*(-[a-zA-Z0-9]+)?)?)(,.*)?\\]`);
    };
  }

  private buildRegex(project: string) {
    return (v: string) => {
      return new RegExp(`\\[${this.sanitize(v)}\\|(((.*,)?${this.sanitize(project)}(,.*)?)|(\\*{1,2}))\\]`);
    };
  }

  private sanitize(s: string): string {
    return s.replace(/[|\\{}()[\]^$+*?.]/g, '\\$&');
  }
}

export default VersionResolver;