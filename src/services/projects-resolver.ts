import { dirname } from 'path';
import { Config } from '../models/config';
import { ProjectFiles } from '../models/project-files';

export interface IProjectsResolverService {
  getProjects(config: Config): ProjectFiles;
}

class ProjectsResolverService implements IProjectsResolverService {
  constructor(private searchFiles: (pattern: string) => string[]) {
  }

  getProjects(config: Config): ProjectFiles {
    const reducer = (acc: string[], curr: string) => acc.concat(this.searchFiles(curr));
    const projects = config.projects.include.reduce(reducer, []);
    const excludes = config.projects.exclude.reduce(reducer, []);
    const shared = config.projects.shared.reduce(reducer, []);

    const allProjects = projects.filter((x) => !excludes.includes(x));
    const filesPerProject = allProjects.reduce<ProjectFiles>((acc, curr) => {
      const directory = dirname(curr);
      acc[directory] = this.searchFiles(`${directory}/**/*`)
        .map((v) => v.replace(/^\.\//, ''))
        .concat(shared)
        .filter((v) => !excludes.includes(v));
      return acc;
    }, {});

    return filesPerProject;
  }
}

export default ProjectsResolverService;