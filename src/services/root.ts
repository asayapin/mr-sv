import { Args } from '../models/args';
import { IArgsService } from './args-service';
import HelpService from './help-service';
import InitService from './init-service';
import MainService from './main-service';

class RootService {
  constructor(private argsService: IArgsService, 
              private helpService: HelpService, 
              private mainService: MainService, 
              private initService: InitService) { }

  public async run() {
    const args = this.argsService.getArgs();
    switch (args.command) {
      case 'run':
        return this.runMain();
      case 'init':
        return this.runInit(args);
      case 'help':
        return this.runHelp();
      default:
        return this.runHelp();
    }
  }

  private async runInit(args: Args) {
    return this.initService.writeSampleConfig(args);
  }

  private async runHelp() {
    console.log(this.helpService.getHelp());
    return Promise.resolve();
  }

  private async runMain() {
    return this.mainService.run();
  }

  public static getHelp(): string {
    return `mr-sv: monorepository SemVer version generator
Author: Andrey Sayapin

Usage: node mr-sv <command> <args>

Available commands:
    run  - process current repository
    init - create sample config
    help - outputs this message

Common arguments:
    -c, --config        Path to config file`;
  }
}

export default RootService;
