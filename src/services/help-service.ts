import InitService from './init-service';
import MainService from './main-service';
import RootService from './root';

class HelpService {
  getHelp(): string {
    const helps = [RootService.getHelp(), InitService.getHelp(), MainService.getHelp()];
    return helps.join('\n\n\n');
  }
}

export default HelpService;
