import { exit } from 'process';
import { RootServiceFactory } from './service-factory';

const rootService = RootServiceFactory();
rootService.run().catch((r) => {
  console.log(r);
  exit(1);
});

/**
 * flow (apporximate):
 * - [x] read config & deserialize in inner representation
 * - [x] process config
 *   - [x] find projects
 *     - [x] get all files in current working directory
 *     - [x] remove using `exclude` node
 *     - [x] get projects using `include` node
 *     - [x] for every project, assume it's files are located in directory containing the project file (or the folder found)
 *     - [x] for every project, create list of files included in this project (including global includes)
 *   - [x] prepare regexes
 *     - [x] for version update markers
 *   - [x] prepare update specifiers
 * - [x] get git log
 * - [ ] split commits among projects
 *   - [x] determine if commit belong to a project
 *   - [ ] determine version update involevd by commit
 *     - [ ] process override instruction
 * - [x] reverse project-scoped histories
 * - [x] build projects version
 * - [x] export versions to envvars
 * - [x] for every project, run command specified
 */
