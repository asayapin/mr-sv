import { promises } from 'fs';
import { GitLogEntry } from '../src/models/git-log-entry';
import VersionResolver, { IVersionResolver } from '../src/services/version-resolver';
import { MockConfigService } from './mocks/config-service';
import { MockGitService } from './mocks/git-service';
import { MockProjectResolverService } from './mocks/project-resolver-service';

describe('version resolver', () => {
    test.each`
    ver1            | ver2          | log
    ${'2.0.0.0'}    | ${'2.0.0.0'}  | ${'[maj|*] bumps all regardless of files'}
    ${'1.0.1.0'}    | ${'1.0.1.0'}  | ${'[fix|*] bumps all regardless of files$p3/q'}
    ${'1.0.0.1'}    | ${'1.0.0.1'}  | ${'inc build both$p1/a$p2/a'}
    ${'1.0.0.1'}    | ${'1.0.0.0'}  | ${'inc build both$p1/a$p3/a.cs'}
    ${'1.0.0.0'}    | ${'1.0.0.1'}  | ${'inc build both$p3/a.cs$p2/a'}
    ${'1.0.1.0'}    | ${'1.0.0.1'}  | ${'[fix|p1] drop build$p1/a$$inc build both$p1/a$p2/a'}
    ${'1.1.0.0'}    | ${'1.0.0.1'}  | ${'[min|p1] drop patch$p1/a$$[fix|p1] drop build$p1/a$$inc build both$p1/a$p2/a'}
    ${'2.0.0.0'}    | ${'1.0.0.1'}  | ${'[maj|p1] drop minor$p1/a$$[min|p1] drop patch$p1/a$$[fix|p1] drop build$p1/a$$inc build both$p1/a$p2/a'}
    ${'1.0.0.0'}    | ${'1.0.0.1'}  | ${'[fix|p1] wrong scope$p2/a'}
    ${'1.0.0.0'}    | ${'1.0.1.0'}  | ${'[fix|p1,p2] wrong scope$p2/a'}
    ${'1.0.0.0'}    | ${'1.0.0.0'}  | ${'[fix|p1,p2] wrong scope$p3/a.cs'}
    ${'1.0.1.0'}    | ${'1.0.1.0'}  | ${'[fix|p1,p2] shared specified$p1/a$p2/a'}
    ${'2.0.0.0'}    | ${'3.0.0.0'}  | ${'[ovr|p1:2.0.0,p2:3.0.0] override$p1/a$p2/a'}
    ${'1.1.0.0'}    | ${'1.1.0.0'}  | ${'[min|**] bumps all$p1/a$p2/a'}
    ${'1.1.0.0'}    | ${'1.0.0.0'}  | ${'[min|**] bumps p1$p1/a'}
    ${'1.0.0.0'}    | ${'1.1.0.0'}  | ${'[min|**] bumps p2$p2/a'}
    ${'1.0.0.0'}    | ${'1.0.0.0'}  | ${'[min|**] bumps none$p3/a'}
    `('on <$log> yields $ver1 and $ver2', ({ver1, ver2, log}) => {
        var resolver : IVersionResolver = new VersionResolver(
            new MockConfigService('1.0.0'),
            new MockProjectResolverService(),
            new MockGitService(log)
        );

        const versions = resolver.resolveVersions();
        const p1 = versions.then(v => v['p1'].toString()).catch(console.log);
        const p2 = versions.then(v => v['p2'].toString()).catch(console.log);

        expect(p1).resolves.toBe(ver1);
        expect(p2).resolves.toBe(ver2);
    });
});