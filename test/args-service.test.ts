import ArgsService from '../src/services/args-service';

describe('args parser tests', () => {
    test.each`
    config                     | command   | drop     | args
    ${'config.json'}           | ${'help'} | ${false} | ${[]}
    ${'config.json'}           | ${'run'}  | ${false} | ${['run']}
    ${'test'}                  | ${'run'}  | ${false} | ${'-c test run'.split(' ')}
    ${'config.json'}           | ${'init'} | ${true}  | ${'--drop-build-number init'.split(' ')}
    ${'config.json'}           | ${'run'}  | ${false} | ${'junk junk junk run'.split(' ')}
    ${'test'}                  | ${'run'}  | ${true}  | ${'junk run junk -c test junk --drop-build-number'.split(' ')}
    ${'config.json'}           | ${'help'} | ${false} | ${'junk junk junk'.split(' ')}
    ${'test'}                  | ${'run'}  | ${true}  | ${'run -c test --drop-build-number'.split(' ')}
    `('on $args', ({config, command, drop, args}) => {
        const argsService = new ArgsService(() => args);
        const a = argsService.getArgs();

        expect(a.command).toBe(command);
        expect(a.config).toBe(config);
        expect(a.dropBuildNumber).toBe(drop);
    });

    it('crashes on option and switch', () => {
        const argsService = new ArgsService(() => '-c --drop-build-number'.split(' '));

        expect(() => argsService.getArgs()).toThrow();
    });
});