import {Version} from '../src/models/version';

describe('version tests', () => {
    test.each`
    version             | maj   | min   | fix   | build | chunk
    ${'1.2.3'}          | ${1}  | ${2}  | ${3}  | ${0}  | ${''}
    ${''}               | ${0}  | ${0}  | ${0}  | ${0}  | ${''}
    ${'1'}              | ${1}  | ${0}  | ${0}  | ${0}  | ${''}
    ${'1.2'}            | ${1}  | ${2}  | ${0}  | ${0}  | ${''}
    ${'1.2.3.4'}        | ${1}  | ${2}  | ${3}  | ${4}  | ${''}
    ${'1.2.3.4-alpha'}  | ${1}  | ${2}  | ${3}  | ${4}  | ${'alpha'}
    `('on $version', ({version, maj, min, fix, build, chunk}) => {
        const v = new Version(version);

        expect(v.major).toBe(maj);
        expect(v.minor).toBe(min);
        expect(v.patch).toBe(fix);
        expect(v.build).toBe(build);
        expect(v.chunk).toBe(chunk);
    });
});