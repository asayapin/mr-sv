import { Config } from "../../src/models/config";
import { ProjectFiles } from "../../src/models/project-files";
import { IProjectsResolverService } from "../../src/services/projects-resolver";

export class MockProjectResolverService implements IProjectsResolverService{
    getProjects(config: Config): ProjectFiles {
        return {
            'p1':[
                'p1/a',
                'p1/p1.csproj',
                'p1/b.cs'
            ],
            'p2':[
                'p2/a',
                'p2/p2.csproj',
                'p2/b.cs'
            ],
        };
    }

}