import { GitLogEntry } from "../../src/models/git-log-entry";
import { IGitService } from "../../src/services/git";

export class MockGitService implements IGitService {
    private l: GitLogEntry[];
    constructor(ls: string) {
        this.l = ls.split('$$').map(s => {
            const t = s.split('$');
            return {
                text: t[0],
                files: t.slice(1)
            };
        });
     }
    async log(): Promise<GitLogEntry[]> {
        return this.l;
    }
}