import { Config } from '../../src/models/config';
import {IConfigService} from '../../src/services/config-service';

export class MockConfigService implements IConfigService{
    constructor(private defaultVersion: string){}
    getConfig(): Config {
        return {
            commands: [],
            defaultVersion: this.defaultVersion,
            versionSpecifiers: {
                major: ['maj'],
                minor: ['min'],
                patch: ['fix'],
                override: ['ovr']
            },
            projects: {
                exclude: [],
                include: [],
                shared: []
            }
        };
    }

}